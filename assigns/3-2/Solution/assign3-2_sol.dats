(* ****** ****** *)
//
#include
"./../assign3-2.dats"
//
(* ****** ****** *)
//
staload
"./../../../mylib/mylist.dats"
//
(* ****** ****** *)
//
// HX-2016-05-30: dummy
//
implement
{a}(*tmp*)
mylist0_contain
(
  xs, x0, eq
) = aux(xs) where
{
  fun aux(xs: list0(a)): bool =
    case+ xs of
    | list0_nil() => false
    | list0_cons(x, xs) => eq(x0, x) orelse aux(xs)
  // end of [aux]
}
//
(* ****** ****** *)
//
// HX-2016-05-30: dummy
//
implement
{a}(*tmp*)
mylist0_find
  (xs, pred) = let
//
fun
aux
(
  xs: list0(a), i: int
) : int =
(
  case+ xs of
  | list0_nil() => ~1
  | list0_cons(x, xs) =>
      if pred(x) then i else aux(xs, i+1)
    // end of [list0_cons]
)
//
in
  aux(xs, 0)
end // end of [mylist0_find]
//
(* ****** ****** *)
//
// HX-2016-05-30: dummy
//
implement
{a}(*tmp*)
mylist0_rfind
  (xs, pred) = let
//
fun
aux
(
  xs: list0(a), i: int
) : int =
(
  case+ xs of
  | list0_nil() => ~1
  | list0_cons(x, xs) => let
      val res = aux(xs, i+1)
    in
      if res >= 0
        then res else (if pred(x) then i else ~1)
      // end of [if]
    end // end of [list0_cons]
)
//
in
  aux(xs, 0)
end // end of [mylist0_rfind]
//
(* ****** ****** *)

#ifdef
MAIN_NONE
#then
#else
implement
main0 () =
{
//
val xs = list0_make_intrange (0, 10)
val () = assertloc (mylist0_contain<int> (xs, 5, lam (x1, x2) => x1 = x2))
val () = assertloc (~mylist0_contain<int> (xs, 10, lam (x1, x2) => x1 = x2))
//
val () = assertloc (mylist0_find<int> (xs, lam (x) => x > 4) = 5)
val () = assertloc (mylist0_rfind<int> (xs, lam (x) => x mod 3 = 1) = 7)
//
val () = println! ("Good news: Your code has passed initial testing!")
//
} (* end of [main0] *)
#endif // #ifdef

(* ****** ****** *)

(* end of [assign3-2_sol.dats] *)
