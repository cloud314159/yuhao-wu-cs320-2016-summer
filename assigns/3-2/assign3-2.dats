//
// Title:
// Concepts of Programming Languages
// Number: CAS CS 320
// Semester: Summer I, 2016
// Class Time: MTWR 3:00-5:00
// Instructor: Hongwei Xi (hwxiATbuDOTedu)
// Teaching Fellow: Hanwen Wu (hwwuATbuDOTedu)
//
(*
//
// Assignment #3-2
// Due Tuesday, the 7th of May, 2016 at 11:59pm
//
*)
(* ****** ****** *)

#define
ATS_PACKNAME "CS320_SUMMER15_HW3_2"

(* ****** ****** *)
//
#include
"share/atspre_staload.hats"
//
#include
"share/HATS/atspre_staload_libats_ML.hats"
//
(* ****** ****** *)
//
// 10 points
//
// HX: please check if [x0] occurs in [xs]
//
extern
fun{a:t@ype}
mylist0_contain
  (xs: list0 (a), x0: a, eq: (a, a) -> bool): bool
//
(* ****** ****** *)
//
// 15 points
//
// HX: [mylist0_find] checks if there is an element
// in [xs] that satisfies a given predicate [pred]. If
// such an element is found, it returns the index of the
// element. Otherwise, ~1 (negative 1) is returned.
//
// Note that [mylist0_find] searches from left to right.
//
extern
fun{a:t@ype}
mylist0_find (xs: list0 (a), pred: (a) -> bool): int
//
(* ****** ****** *)
//
// 15 points
//
// HX: [mylist0_rfind] checks if there is an element
// in [xs] that satisfies a given predicate [pred]. If
// such an element is found, it returns the index of the
// element. Otherwise, ~1 (negative 1) is returned.
//
// Note that [mylist0_rfind] searches from right to left.
//
extern
fun{a:t@ype}
mylist0_rfind (xs: list0 (a), pred: (a) -> bool): int
//
(* ****** ****** *)

(* end of [assign3-2.dats] *)
