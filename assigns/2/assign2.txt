//
// Title:
// Concepts of Programming Languages
// Number: CAS CS 320
// Semester: Summer I, 2016
// Class Time: MTWR 3:00-5:00
// Instructor: Hongwei Xi (hwxiATbuDOTedu)
// Teaching Fellow: Hanwen Wu (hwwuATbuDOTedu)
//
(*
//
// Assignment #2
// Due Tuesday, the 31st of May, 2016 at 11:59pm
//
*)
(* ****** ****** *)
//
// HX: 20 points
//
// Assign2-1:
//
// Please see the DigitalClock.dats file in the same
// directory.
//
(* ****** ****** *)
//
// HX: 10 bonus points
//
// Assign2-2:
// Please find a way to install pyname on your own machine.
// A complete installation means that you will get (6, 0) when
// calling pyname.init(), where 6 means 6 passes and 0 means 0
// failures. For each failure, 1 point is deducted.
//
(* ****** ****** *)

(* end of [assign2.txt] *)
