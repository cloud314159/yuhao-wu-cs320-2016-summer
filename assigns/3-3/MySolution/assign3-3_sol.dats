(* ****** ****** *)

#include
"./../assign3-3.dats"

(* ****** ****** *)
//
// HX-2015-06-05: dummy
//
implement
intrep_add (xs, ys) = list0_nil()
//
(* ****** ****** *)
//
// HX-2015-06-05: dummy
//
implement
pow2digitsum(n) = 0

(* ****** ****** *)
//
// HX-2015-06-05: dummy
//
implement
intrep_mul (xs, ys) = list0_nil()
//
(* ****** ****** *)

implement
main0 () = () where
{
//
val () = assertloc(pow2digitsum(1000) = 1366)
//
} (* end of [main0] *)

(* ****** ****** *)

(* end of [assign3-3_sol.dats] *)
