(* ****** ****** *)

#include "./../assign3-3.dats"

(* ****** ****** *)
//
extern
fun
intrep_succ(intrep): intrep
//
extern
fun
intrep_add2(intrep, intrep, c: int): intrep
//
(* ****** ****** *)
//
implement
intrep_add(xs, ys) = intrep_add2 (xs, ys, 0)
//
implement
intrep_succ (xs) =
  case+ xs of
  | nil0() => list0_sing(1)
  | cons0(x, xs) =>
    let
      val x1 = x + 1
    in
      if x1 <= 9 then cons0(x1, xs) else cons0(0, intrep_succ(xs))
    end // end of [let]
//
implement
intrep_add2
  (xs, ys, c) =
  case+ (xs, ys) of
  | (nil0(), _) => if c = 0 then ys else intrep_succ(ys)
  | (_, nil0()) => if c = 0 then xs else intrep_succ(xs)
  | (cons0(x, xs),
     cons0(y, ys)) => let
      val xyc = x + y + c
    in
      if xyc <= 9
        then cons0(xyc, intrep_add2(xs, ys, 0)) else cons0(xyc-10, intrep_add2(xs, ys, 1))
      // end of [if]
    end // end of [let]
//
(* ****** ****** *)
//
// HX-2015-06-05: dummy
//
implement
pow2digitsum(n) = let
//
fun loop (n: int, res: intrep): intrep =
  if n > 0 then loop (n-1, intrep_add(res, res)) else res
//
in
  list0_foldleft<int><int> (loop(n, list0_sing(1)), 0, lam (res, x) => res + x)
end // end of [pow2digitsum]

(* ****** ****** *)
//
extern
fun
intrep_mc (intrep, m: int, c: int): intrep
//
(* ****** ****** *)
//
// HX: m*xs + c
//
implement
intrep_mc(xs, m, c) =
  case+ xs of
  | nil0() => list0_sing(c)
  | cons0(x, xs) => let
      val mxc = m * x + c
    in
      cons0(mxc%10, intrep_mc(xs, m, mxc/10))
    end // end of [cons0]

(* ****** ****** *)
//
implement
intrep_mul (xs, ys) =
(
case+ ys of
| nil0() => nil0()
| cons0(y, ys) =>
  intrep_add(intrep_mc(xs, y, 0), cons0(0, intrep_mul(xs, ys)))
)
//
(* ****** ****** *)

#ifdef
MAIN_NONE
#then
#else
implement
main0 () = () where
{
//
  val
  pow2digitsum_1000 = pow2digitsum(1000)
//
  val () =
  println! ("pow2digitsum(1000) = ", pow2digitsum_1000)
  val _4321_ = cons0(1, cons0(2, cons0(3, cons0(4, nil0()))))
//
  val ((*void*)) =
  assertloc(list0_foldright<int><int>(intrep_mul (_4321_, _4321_), lam(x, res) => x + 10*res, 0)=4321*4321)
//
} (* end of [main0] *)
#endif // ifdef

(* ****** ****** *)

(* end of [assign3-3_sol.dats] *)
