(* ****** ****** *)
//
#include
"./../assign3.dats"
//
(* ****** ****** *)
//
staload
"./../../../mylib/mylist.dats"
(*
staload
"./../../../mylib2/mylist.dats"
*)
//
(* ****** ****** *)
//
// HX-2016-05-30: dummy
//
implement
mylist0_last (xs) = let
//
fun
aux(x0: int, xs: list0(int)) =
(
case+ xs of
| list0_nil() => x0
| list0_cons(x, xs) => aux(x, xs)
)
//
in
  case- xs of list0_cons(x, xs) => aux(x, xs)
end // end of [mylist0_last]

(* ****** ****** *)
//
// HX-2016-05-30: dummy
//
implement
mylist0_length (xs) =
mylist_foldleft_cloref<int,int> (xs, 0, lam(n, _) => n+1)

(* ****** ****** *)
//
// HX-2016-05-30: dummy
//
implement
mylist0_choose(xs, n) =
if
n > 0 then
(
case xs of
| list0_nil() => list0_nil()
| list0_cons(x, xs) =>
    mylist_append(mylist_mcons(mylist0_choose(xs, n-1), x), mylist0_choose(xs, n))
) else list0_sing(list0_nil())
//
(* ****** ****** *)

#ifdef
MAIN_NONE
#then
#else
implement
main0 () = () where
{
//
val xs =
  list0_make_intrange(0, 10)
//
val () =
  assertloc (mylist0_last(xs) = list0_last_exn(xs))
//
val () =
  assertloc (mylist0_length(xs) = list0_length(xs))
//
val nxs = list0_length (xs)
val xss2 = mylist0_choose (xs, 2)
val xss3 = mylist0_choose (xs, 3)
val ((*void*)) =
  assertloc (list0_length(xss2) = nxs * (nxs-1) / 2)
val ((*void*)) =
  assertloc (list0_length(xss3) = nxs * (nxs-1) * (nxs-2) / 6)
//
val () = println! ("Good news: Your code has passed initial testing!")
//
} (* end of [main0] *)
#endif // #ifdef

(* ****** ****** *)

(* end of [assign3_sol.dats] *)
