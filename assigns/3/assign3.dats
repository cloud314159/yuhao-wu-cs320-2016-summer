//
// Title:
// Concepts of Programming Languages
// Number: CAS CS 320
// Semester: Summer I, 2016
// Class Time: MTWR 3:00-5:00
// Instructor: Hongwei Xi (hwxiATbuDOTedu)
// Teaching Fellow: Hanwen Wu (hwwuATbuDOTedu)
//
(*
//
// Assignment #3
// Due Friday, the 3rd of May, 2016 at 11:59pm
//
*)
(* ****** ****** *)
//
#include
"share/atspre_staload.hats"
#include
"share/HATS/atspre_staload_libats_ML.hats"
//
(* ****** ****** *)

#define
ATS_PACKNAME "CS320_SUMMER15_HW3"

(* ****** ****** *)

(*
//
10 points
//
Write a function to return the last element of a nonempty
list. You may assume that the input of this function is a
non-empty list.
*)
extern
fun
mylist0_last (xs: list0(int)): int

(* ****** ****** *)

(*
//
10 points
//
Write a function to return the length of a give list.
It is required that your implementation is tail-recursive.
*)
extern
fun
mylist0_length (xs: list0(int)): int

(* ****** ****** *)

fun
mylist0_mcons
(
  x0: int, xss: list0(list0(int))
) : list0(list0(int)) =
(
  case+ xss of
  | list0_nil () => list0_nil ()
  | list0_cons (xs, xss) =>
      list0_cons (list0_cons(x0, xs), mylist0_mcons(x0, xss))
    // end of [list0_cons]
)

(* ****** ****** *)
(*
//
20 points
//
Given a list xs and an integer n <= length(xs),
[mylist0_choose] returns a list of lists consisting of
all the n-tuples formed using elements in [xs].
//
*)
extern
fun
mylist0_choose
  (xs: list0(int), n: int): list0(list0(int))
//
(* ****** ****** *)

(* end of [assign3.dats] *)
