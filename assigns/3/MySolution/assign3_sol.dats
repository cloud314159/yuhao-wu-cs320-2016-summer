(* ****** ****** *)
//
#include
"./../assign3.dats"
//
(* ****** ****** *)
//
// HX-2016-05-30: dummy
//
implement
mylist0_last (xs) = 0

(* ****** ****** *)
//
// HX-2016-05-30: dummy
//
implement
mylist0_length (xs) = 0

(* ****** ****** *)
//
// HX-2016-05-30: dummy
//
implement
mylist0_choose (xs, n) = list0_nil()

(* ****** ****** *)

#ifdef
MAIN_NONE
#then
#else
implement
main0 () = () where
{
//
val xs =
  list0_make_intrange(0, 10)
//
val () =
  assertloc (mylist0_last(xs) = list0_last_exn(xs))
//
val () =
  assertloc (mylist0_length(xs) = list0_length(xs))
//
val nxs = list0_length (xs)
val xss = mylist0_choose (xs, 2)
val ((*void*)) =
  assertloc (list0_length (xss) = nxs * (nxs-1) / 2)
//
val () = println! ("Good news: Your code has passed initial testing!")
//
} (* end of [main0] *)
#endif // #ifdef

(* ****** ****** *)

(* end of [assign3_sol.dats] *)
