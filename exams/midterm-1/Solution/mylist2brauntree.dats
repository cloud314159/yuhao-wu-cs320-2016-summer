(* ****** ****** *)

#include "./../midterm-1.dats"

(* ****** ****** *)

implement
{a}(*tmp*)
mylist2brauntree
  (xs) = let
//
val n = length(xs)
//
in
//
  if n >= 1 then let
    val n2 = n / 2
    val ys1 = list0_take_exn(xs, n2)
    val-cons0(y0, ys2) = list0_drop_exn(xs, n2)
  in
    tree0_cons(mylist2brauntree(ys1), y0, mylist2brauntree(ys2))
  end else tree0_nil(*void*)
//
end // end of [mylist2brauntree]

(* ****** ****** *)

implement main0() = ()

(* ****** ****** *)

(* end of [mylist2brauntree.dats] *)
