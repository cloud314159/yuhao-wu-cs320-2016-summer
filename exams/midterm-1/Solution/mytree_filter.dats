(* ****** ****** *)

#include "./../midterm-1.dats"

(* ****** ****** *)
//
implement
{a}(*tmp*)
mytree_filter(xs, pred) =
  mytree_fold<a,list0(a)>
  ( xs
  , lam(xs_l, x0, xs_r) => 
    if pred(x0) then xs_l+cons0(x0, xs_r) else xs_l+xs_r
  , list0_nil((*void*))
  )
//
(* ****** ****** *)

implement main0() = ()

(* ****** ****** *)

(* end of [mytree_filter.dats] *)
