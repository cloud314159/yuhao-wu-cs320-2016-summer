(* ****** ****** *)

#include "./../midterm-1.dats"

(* ****** ****** *)
//
implement
{a}(*tmp*)
mytree_maxminHT
  (xs) =
(
  mytree_fold<a,int>
  ( xs
  , lam(hhl, _, hhr) => 
    $tup(1+max(hhl.0, hhr.0), 1+min(hhl.0, hhr.0))
  , $tup(0, 0)
  )
)
//
(* ****** ****** *)

implement main0() = ()

(* ****** ****** *)

(* end of [mytree_maxminHT.dats] *)
