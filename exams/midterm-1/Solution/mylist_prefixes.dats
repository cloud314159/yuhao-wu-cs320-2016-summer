(* ****** ****** *)

#include "./../midterm-1.dats"

(* ****** ****** *)

implement
{a}(*tmp*)
mylist_prefixes
  (xs) =
(
  mylist_foldright_cloref<a,list0(list0(a))>
  ( xs
  , lam(x, res) => cons0(nil0, mylist_mcons<a>(res, x))
  , list0_nil(*void*)
  )
)
(* ****** ****** *)

implement
main0() = () where
{
//
val xs =
  g0ofg1($list{int}(1,2,3,4,5))
//
val () = println! ("xs = ", xs)
val () = println! ("prefixes(xs) = ", mylist_prefixes<int>(xs))
//
} (* end of [main0] *)

(* ****** ****** *)

(* end of [mylist_prefixes.dats] *)
