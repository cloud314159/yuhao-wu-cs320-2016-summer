(* ****** ****** *)

#include "./../midterm-1.dats"

(* ****** ****** *)
//
// HX:
// A dummy implementation
//
implement sublist_test(xs, ys) = true
//
(* ****** ****** *)

implement
main0() = () where
{
//
val xs =
  g0ofg1($list{int}(1,2,3,4,5))
//
val ys = g0ofg1($list{int}(1,3,5))
val zs = g0ofg1($list{int}(1,2,2))
//
val () = assertloc(sublist_test(xs, ys))
val () = assertloc(not(sublist_test(xs, zs)))
//
} (* end of [main0] *)

(* ****** ****** *)

(* end of [sublist_test.dats] *)
