//
// Title:
// Concepts of Programming Languages
// Number: CAS CS 320
// Semester: Summer 2015
// Class Time: TR 3:00-5:00pm
// Instructor: Hongwei Xi (hwxiATbuDOTedu)
// Teaching Fellow: Zhiqiang (Alex) Ren (arenATbuDOTedu)
//
(* ****** ****** *)
//
// HX-2015-06-04:
//
// There are 10 questions and each question is worth 10 points.
// There are 20 points in the 100 total points that are considered
// bonus points.
//
// Your overall score is calculated according to the following
// formula:
//
// (number-of-points-you-earn/80)*100%
//
(* ****** ****** *)

staload "libats/ML/SATS/basis.sats"

(* ****** ****** *)
//
// Midterm 1
//
(* ****** ****** *)
(*
//
Q1: 10 points
//
The function [mylist0_max] returns the
maximum of x0 and all the elements in xs
//
*)
//
fun mylist0_max(x0: int, xs: list0(int)): int
//
(* ****** ****** *)

(*
//
Q2: 10 points
//
Please implement a function [drawX] to print out
the following patterns.
//
drawX (0) should output
*
//
drawX (1) should output
* *
 *
* *
//
drawX (2) should output
*   *
 * *
  *
 * *
*   *
//
*)
fun drawX (n: int): void
     
(* ****** ****** *)

(*
//
Q3: 10 points
//
Please implement a function int2binary to turn
a given number into its binary representation.
For instance,
//
int2binary(0) = 0 :: nil0()
int2binary(1) = 1 :: nil0()
int2binary(4) = 1 :: 0 :: 0 :: nil0()
int2binary(10) = 1 :: 0 :: 1 :: 0 :: nil0()
//
You can assume that the input number is non-negative.
//
*)
fun int2binary (x: int): list0 (int)

(* ****** ****** *)
//
(*
//
Q4: 10 points
//
Given a list of numbers:

x1, x2, ..., xn

the function [mylist0_average] returns another list of numbers

y1, y2, ..., yn

such that

y1 = x1, y2 = (x1+x2)/2, ..., yn = (x1+x2+...+xn)/n

For instance,

(1.0, 2.0, 3.0) is transformed into (1.0, 1.5, 2.0)
//
*)
fun mylist0_average (xs: list0 (double)): list0 (double)
//
(* ****** ****** *)

(*
//
Q5: 10 points
//
The function [mylist0_isord] checks whether a given list of
integers is ordered (according to the less-than-or-equal-to
(<=) ordering). For instance, (1, 2, 2, 3, 5) is ordered but
(1, 3, 2, 2, 4) is not.
*)
fun mylist0_isord (xs: list0 (int)): bool

(* ****** ****** *)
//
(*
//
Q6: 10 points
//
** [mylist0_merge] merges two given lists alternately.
** For instance, if the given lists are [1,3,5] and [2,4],
** the result is [1, 2, 3, 4, 5]
*)
//
fun{
a:t@ype
} mylist0_merge (list0(a), list0(a)): list0(a)
//
(* ****** ****** *)
//
(*
//
Q7: 10 points
//
** [mylist0_split] splits a list alternately.
** For instance, if the given list is [1, 2, 3, 4, 5]
** then it is splitted into [1,3,5] and [2,4]
*)
//
fun{
a:t@ype
} mylist0_split (xs: list0(a)): (list0(a), list0(a))
//
(* ****** ****** *)
//
datatype
tree0 (a:t@ype) =
  | tree0_nil of ()
  | tree0_cons of (tree0 a, a, tree0 a)
//
(* ****** ****** *)
//
(*
//
Q8: 10 points
//
The function [mytree0_tally] returns the total of all
the integers contained in a given tree:
//
*)
fun mytree0_tally(xs: tree0(int)): int
//
(* ****** ****** *)
//
(*
//
Q9: 10 points
//
The function [tree0_flatten] returns a list consisting
of all the elements contained in a given tree:
//
*)
fun{a:t@ype} mytree0_flatten(xs: tree0(a)): list0(a)
//
(* ****** ****** *)

(*
//
Q10: 10 points
//
The function substring_test(str1, str2) checks whether
string str1 contains string str2 as a substring.
For instance, substring_test("computer", "put") returns
true, and substring_test("computer", "come") returns false.
//
*)
fun substring_test(str1: string, str2: string): bool

(* ****** ****** *)

(* end of [midterm1.sats] *)
