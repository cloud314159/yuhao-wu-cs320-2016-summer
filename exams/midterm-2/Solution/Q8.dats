(* ****** ****** *)

#include
"share/atspre_staload.hats"
#include
"share/HATS/atspre_staload_libats_ML.hats"

(* ****** ****** *)

staload
"./../../../mylib/mylist.dats"

(* ****** ****** *)

#include "./../midterm-2.dats"

(* ****** ****** *)

implement 
find_breakups(n) = let
//
fun
aux
(
  i: int, n: int
) : list0(list0(int)) =
  if n = 0 then list0_sing(nil0()) else
    (if n >= i then mylist_mcons(aux(i, n-i), i)+aux(i+1, n) else nil0())
  // end of [n = 0]
//
in
  aux(1, n)
end // end of [find_breakups]

(* ****** ****** *)

implement
main0((*void*)) =
{
  val () = println! ("find_breakups(6) = ", find_breakups(6))
}

(* ****** ****** *)

(* end of [Q8.dats] *)
