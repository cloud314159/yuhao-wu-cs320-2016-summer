//
// Title:
// Concepts of Programming Languages
// Number: CAS CS 320
// Semester: Summer I, 2016
// Class Time: MTWR 3:00-5:00
// Instructor: Hongwei Xi (hwxiATbuDOTedu)
// Teaching Fellow: Hanwen Wu (hwwuATbuDOTedu)
//
(* ****** ****** *)
//
// Midterm 2
//
(* ****** ****** *)
//
// Out: 12:00 on the 23rd of June, 2016
// Due: 11:59pm on the 24th of June, 2016
//
(* ****** ****** *)
//
// ATTENTION:
//
// Absolutely no collaboration in any form is allowed!!!
//
// Turning in code copied from an on-line source (without
// revealing the source) violates BU's academic standard!
//
(* ****** ****** *)
//
// HX-2016-06-23:
// There are 120 points in total, and 100% = 80 points
//
(* ****** ****** *)

staload "libats/ML/SATS/basis.sats"

(* ****** ****** *)
//
// Midterm-2
//
(* ****** ****** *)
//
datatype
tree0 (a:t@ype) =
  | tree0_nil of ()
  | tree0_cons of (tree0 a, a, tree0 a)
//
(* ****** ****** *)
(*
//
// Q1: 10 points
//
// [mylist_duprem_l] keeps the leftmost occurrence
// of each element and removes all of its duplicates
//
// [mylist_duprem_r] keeps the rightmost occurrence
// of each element and removes all of its duplicates
//
// For instance,
// mylist_duprem_l([1,2,3,4,3,2,1]) returns [1,2,3,4]
// mylist_duprem_r([1,2,3,4,3,2,1]) returns [4,3,2,1]
//
*)
//
extern
fun
mylist_duprem_l(xs: list0(int)): list0(int)
//
extern
fun
mylist_duprem_r(xs: list0(int)): list0(int)
//
(* ****** ****** *)
//
(*
//
// Q2: 10 points
//
Please implement a function which checks whether a binary tree is a
max-heap.  A binary tree is a max-heap if it is empty or its root is larger
than or equal to all the other nodes in it and its left and right sub-trees
are also max-heaps.
//
*)
extern
fun
tree0_is_maxheap (t0: tree0 (int)): bool
//
(* ****** ****** *)
(*
**
** Q3: 10 points
**
*)
(*
You can find Wallis product here:
http://en.wikipedia.org/wiki/Wallis_product
Please implement a function that generates a stream
consisting of all the partial products in Wallis product:
*)
//
extern fun Wallis((*void*)): stream(double)
//
(* ****** ****** *)
//
(*
//
// Q4: 10 points
//
// Given a list of positive numbers xs and an integer n,
// [sstally_test] returns true if and only if there is
// a subsequence of xs such that the sum of all the integers
// in this subsequence equals n. For instance, we have
// 
// sstally_test([1,2,3,4], 7) returns true
// sstally_test([1,2,3,4], 11) returns false
//
*)
//
extern
fun
sstally_test (xs: list0(int), n: int): bool
//
(* ****** ****** *)

(*
//
// Q5: 10 points
//
// Given a non-empty list of integers, mylist_freqfind
// returns the one that appears most frequently in this list.
// If there are several integers satisfying the criterion,
// please return the largest one among them.
// For instance, we have
//
// mylist_freqfind([1,3,1,1,2,2,4,2,3]) returns 2
//
*)
extern fun mylist_freqfind(xs: list0(int)): int

(* ****** ****** *)
//
(*
//
// Q6: 10 points
//
// Given one infinite streams
// xs = x_0, x_1, x_2, ...
// Please generate another stream xxs that
// consists of all the pairs (x_i, x_j) for 0 <= i < j
//
*)
extern
fun
{a:t@ype}
stream_pair(xs: stream(a)): stream($tup(a, a))
//
(* ****** ****** *)
//
(*
//
// Q7: 10 points
//
// Given two infinite streams
// xs = x_0, x_1, x_2, ...
// and
// ys = y_0, y_1, y_2, ...
//
// Please generate the cross product of xs and ys that
// consists of all the pairs (x_i, y_j) for i >= 0 and j >= 0.
//
*)
extern
fun
{a,b:t@ype}
stream_xprod
  (xs: stream(a), ys: stream(b)): stream($tup(a, b))
//
(* ****** ****** *)
//
(*
//
// Q8: 10 points
//
Given a nonnegative integer n, a breakup of n is an
ordered sequence of positive integers whose sum equals n.
For instance, (1,1,2,2), (1,2,3), (2,2,2), (3,3) are all
breakups of 6. Please implement a function that returns
all the breakups of a given integer n.
*)
extern
fun find_breakups(n: int): list0(list0(int))
//
(* ****** ****** *)

(*
** Q9: 20 points
*)
//
abstype mymat = ptr // a 2D matrix of doubles
//
extern
fun mymat_get_at (mymat, i: int, j: int): double
and mymat_remove_row (M: mymat, nrow: int): mymat
and mymat_remove_col (M: mymat, ncol: int): mymat
//
// Please implement the following function based on the
// above ones (which are already implemented for you):
//
extern
fun mymat_eval_det (M: mymat, n: int): double
//
// which evaluates the determinant of a given square matrix M
// of dimension n by n.
// See http://en.wikipedia.org/wiki/Determinant for the definition
// of the determinant of a square matrix and the Laplace's formula
// for computing its determinant.

(* ****** ****** *)
//
(*
//
// Q10: 20 points
//
// The permutations of 0,1,2 can be ordered
// according to the lexicographic ordering as follows:
//
// (0,1,2) < (0,2,1) < (1,0,2) < (1,2,0) < (2,0,1) < (2,1,0)
//
// This ordering can be readily generalized to the permuations
// of n numbers, which n is positive. Given a permutation xs of
// the first n natural numbers, perm_succ(xs) returns the next
// permutation following xs or it raises an exception LastPermExn().
// Please implement [perm_succ].
//
*)
//
exception LastPermExn of ()
//
extern fun perm_succ(xs: list0(int)): list0(int)
//
(* ****** ****** *)

(* end of [midterm-2.dats] *)
